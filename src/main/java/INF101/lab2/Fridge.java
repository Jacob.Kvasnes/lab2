package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    int maxCapacity = 20;
    ArrayList<FridgeItem> fridgeLoot = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return this.fridgeLoot.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (this.maxCapacity > this.fridgeLoot.size()){
			this.fridgeLoot.add(item);
            return true;
		}
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(this.fridgeLoot.contains(item)){
            this.fridgeLoot.remove(this.fridgeLoot.indexOf(item));
        } else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        this.fridgeLoot.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        LocalDate date = LocalDate.now();
        for (FridgeItem i : fridgeLoot){
            if (date.isAfter(i.expirationDate)){
                expiredItems.add(i);
            }
        }
        for (FridgeItem i : expiredItems){
            this.takeOut(i);
            System.out.println(i.expirationDate);
        }
        return expiredItems;
    }

}
